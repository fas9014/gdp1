// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The worm model
#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"


//Wormdetails
// Data defining the worm
int theworm_wormpos_y[WORM_LENGTH];  // y-coordinate of the worm
int theworm_wormpos_x[WORM_LENGTH];  // x-coordinate of the worm

int theworm_maxindex;                // Maximale Wurmlänge
int theworm_headindex;               // Kopf des Wurms

// The current heading of the worm
// These are offsets from the set {-1,0,+1}
int theworm_dx;
int theworm_dy;

enum ColorPairs theworm_wcolor;

//Initialize the worm
extern enum ResCodes initializeWorm(struct worm* aworm, int len_max, int len_cur, struct pos headpos, enum WormHeading dir, enum ColorPairs color) {
  int i;
  aworm->maxindex = len_max -1;

  // Current last usable index in array. May grow upto maxindex
  aworm->cur_lastindex = len_cur - 1;
  aworm ->headindex = 0;

  //Initialize position of worms head
  for(i = aworm->headindex; i <= aworm->maxindex; i++) {
    aworm->wormpos[i].y = UNUSED_POS_ELEM;
    aworm->wormpos[i].x = UNUSED_POS_ELEM;
  }

  aworm->wormpos[aworm->headindex] = headpos;

  //Initialize the heading of the worm
  setWormHeading(aworm, dir);

  //Initialze color of the worm
  aworm->wcolor = color;

  return RES_OK;
}

extern void showWorm(struct board* aboard, struct worm* aworm) {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed

  int tailindex;
  tailindex = (aworm->headindex + 1) % (aworm->cur_lastindex + 1);
  int i;
  i = aworm->headindex;

  /*  placeItem(aboard,
      aworm->wormpos[aworm->headindex].y,
      aworm->wormpos[aworm->headindex].x,
      BC_USED_BY_WORM,
      SYMBOL_WORM_HEAD_ELEMENT,
      aworm->wcolor);
      i = (i + aworm->headindex) % (aworm->headindex + 1);
      */
  //  i = (i + aworm->headindex) % (aworm->headindex + 1);
  do {
    /*  if(tailindex == i) {
        placeItem(aboard,
        aworm->wormpos[tailindex].y,
        aworm->wormpos[tailindex].x,
        BC_USED_BY_WORM,
        SYMBOL_WORM_TAIL_ELEMENT,
        aworm->wcolor);
        i = (i + aworm->headindex) % (aworm->headindex + 1);

        } else {
        */
    placeItem(aboard,
        aworm->wormpos[i].y,
        aworm->wormpos[i].x,
        BC_USED_BY_WORM,
        SYMBOL_WORM_INNER_ELEMENT,
        aworm->wcolor);

    i = (i + aworm->cur_lastindex) % (aworm->headindex + 1);

    //  }

  } while(i != aworm->headindex);



  placeItem(aboard,
      aworm->wormpos[aworm->headindex].y,
      aworm->wormpos[aworm->headindex].x,
      BC_USED_BY_WORM,
      SYMBOL_WORM_HEAD_ELEMENT,
      aworm->wcolor);

  placeItem(aboard,
      aworm->wormpos[tailindex].y,
      aworm->wormpos[tailindex].x,
      BC_USED_BY_WORM,
      SYMBOL_WORM_TAIL_ELEMENT,
      aworm->wcolor);



}

void moveWorm(struct board* aboard, struct worm* aworm, enum GameStates* agame_state) {
  // Compute and store new head position according to current heading.
  struct pos headpos;

  headpos = aworm->wormpos[aworm->headindex];

  headpos.x += aworm->dx;
  headpos.y += aworm->dy;




  //Check if we would hit something (for good or bad) or are going to leave
  //the display if we move the worm’s head according to worm’s
  //last direction.
  //We are not allowed to leave the display’s window.

  if(headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastColOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRowOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.

    // Check if the worms head hits any items at the new position on the board.
    // Hitting food is good, hitting barriers or worm elements is bad.
    switch ( getContentAt(aboard,headpos) ) {
      case BC_FOOD_1:
        *agame_state = WORM_GAME_ONGOING;
        // Grow worm according to food item digested
        growWorm(aworm, BONUS_1);
        decrementNumberOfFoodItems(aboard);
        break;
      case BC_FOOD_2:
        *agame_state = WORM_GAME_ONGOING;
        // Grow worm according to food item digested
        growWorm(aworm, BONUS_2);
        decrementNumberOfFoodItems(aboard);
        break;
      case BC_FOOD_3:
        *agame_state = WORM_GAME_ONGOING;
        // Grow worm according to food item digested
        growWorm(aworm, BONUS_3);
        decrementNumberOfFoodItems(aboard);
        break;
      case BC_BARRIER:
        // That’s bad: stop game
        *agame_state = WORM_CRASH;
        break;
      case BC_USED_BY_WORM:
        // That’s bad: stop game
        *agame_state = WORM_CROSSING;
        break;
      default:
        // Without default case we get a warning message.
        {;} // Do nothing. C syntax dictates some statement, here.
    }
  }

  // Check the status of *agame_state
  // Go on if nothing bad happened
  if ( *agame_state == WORM_GAME_ONGOING ) {
    // So all is well: we did not hit anything bad and did not leave the
    // window. --> Update the worm structure.
    // Increment headindex
    // Go round if end of worm is reached (ring buffer)
    aworm->headindex++;
    if (aworm->headindex > aworm->cur_lastindex) {
      aworm->headindex = 0;
    }
    // Store new coordinates of head element in worm structure
    aworm->wormpos[aworm->headindex] = headpos;
  }
}



// Setters
extern void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
    case WORM_UP :// User wants up
      aworm->dx = 0;
      aworm->dy = -1;
      break;

    case WORM_DOWN :// User wants down
      aworm->dx = 0;
      aworm->dy = 1;
      break;

    case WORM_LEFT      :// User wants left
      aworm->dx = -1;
      aworm->dy = 0;
      break;

    case WORM_RIGHT      :// User wants right
      aworm->dx = 1;
      aworm->dy = 0;
      break;
  }
}

extern void cleanWormTail(struct board* aboard, struct worm* aworm) {
  int tailindex;
  tailindex = (aworm->headindex + 1) % (aworm->cur_lastindex + 1);

  if(aworm->wormpos[tailindex].y != UNUSED_POS_ELEM) {
    placeItem(aboard, 
        aworm->wormpos[tailindex].y,
        aworm->wormpos[tailindex].x,
        BC_FREE_CELL,
        SYMBOL_FREE_CELL,
        aworm->wcolor);
  }
}

void growWorm(struct worm* aworm, enum Boni growth) {
  // Play it save and inhibit surpassing the bound
  if (aworm->cur_lastindex + growth <= aworm->maxindex) {
    aworm->cur_lastindex += growth;
  } else {
    aworm->cur_lastindex = aworm->maxindex;
  }
}

extern bool isInUseByWorm(struct worm* aworm, struct pos pos) {
  int i;
  bool collision = false;
  i = aworm->headindex;
  do {
    if(aworm->wormpos[i].y == pos.y && aworm->wormpos[i].x == pos.x) {
      //Kollision
      collision = true;
      break;
    }
    i = (i + aworm->maxindex) % (aworm->maxindex + 1);
  } while(i != aworm->headindex && aworm->wormpos[i].y != UNUSED_POS_ELEM);


  return collision;
}

// Getters
struct pos getWormHeadPos(struct worm* aworm) {
  // Structures are passed by value!
  // -> we return a copy here
  return aworm->wormpos[aworm->headindex];
}

int getWormLength(struct worm* aworm) {
  return aworm->cur_lastindex + 1  ;
}
